<template>
  <v-container fluid class="px-0">
    <v-row class="pt-14 px-3" justify="start" align="center">
      <v-col class="pa-0">
        <div class="text-left">
          <Title :items="route" />
        </div>
      </v-col>
      <v-col class="pa-0">
        <div class="d-flex justify-end">
          <v-btn
            width="auto"
            meduim
            depressed
            color="text-capitalize white--text primary"
            @click="addPointValue()"
          >
            Add Point Value
          </v-btn>
        </div>
      </v-col>
    </v-row>
    <v-row
      justify="center"
      align="start"
      class="d-flex justify-space-between px-0 mt-10"
    >
      <v-col cols="12" lg="2" md="3" sm="5" class="py-1">
        <Subtitle title="Point Value of 1 KIT Point" icon="mdi-currency-usd" />
      </v-col>
      <v-col cols="12" lg="8" md="9" sm="7" class="py-1">
        <Search v-model="search" />
      </v-col>
      <v-col cols="12" lg="2" md="12" class="py-1">
        <div class="d-flex flex-row align-center justify-end">
          <div
            class="body-2 grey--text text--darken-3"
            :style="'width:' + width"
          >
            Per Page<span class="px-1">:</span>
          </div>
          <v-card-actions
            align="center"
            justify="center"
            class="body-2 pl-5 pa-0 pr-0"
            style="width: 100%"
          >
            <v-select
              class="align-center justify-center pa-0 subtitle-2"
              :items="pages"
              item-text="name"
              item-value="id"
              dense
              outlined
              :value="itemsPerPage"
              transition="slide-y-reverse-transition"
              :hide-details="true"
              @change="itemsPerPage = parseInt($event, 10)"
            >
            </v-select>
          </v-card-actions>
        </div>
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        <v-card
          width="100%"
          tile
          outlined
          style="border-top: 3px solid #5a90bf"
        >
          <v-snackbar
            rounded="pill"
            width="40px"
            v-model="snackbarAdd"
            :timeout="timeout"
            color="#578DD2"
          >
            <div class="px-16">
              <v-icon color="white" class="px-6">mdi-check-circle</v-icon>
              Point Value Added !!!
            </div>
          </v-snackbar>
          <v-snackbar
            rounded="pill"
            width="40px"
            v-model="snackbarUpdate"
            :timeout="timeout"
            color="#578DD2"
          >
            <div class="px-16">
              <v-icon color="white" class="px-6">mdi-check-circle</v-icon>
              Point Value Updated !!!
            </div>
          </v-snackbar>
          <v-snackbar
            rounded="pill"
            width="40px"
            v-model="snackbarDelete"
            :timeout="timeout"
            color="#D45252"
          >
            <div class="px-16">
              <v-icon color="white" class="px-6">mdi-check-circle</v-icon>
              Point Value Deleted !!!
            </div>
          </v-snackbar>
          <v-data-table
            :headers="headers"
            :items="kit_point_value"
            hide-default-footer
            class="px-0"
            :search="search"
            :page.sync="page"
            :items-per-page="itemsPerPage"
            @page-count="pageCount = $event"
          >
            <template v-slot:top>
              <v-dialog v-model="dialog" :max-width="width">
                <v-card tile outlined class="py-3">
                  <v-card-title class="px-11 pt-5">
                    <div style="font-size: 22px; color: #627080">
                      {{ formTitle }} Point Value
                      <div
                        style="font-size: 14px"
                        class="py-0 font-weight-regular"
                      >
                        {{ formSubtitle }} Point Value Form
                      </div>
                    </div>
                  </v-card-title>
                  <v-card-text>
                    <v-container fluid>
                      <v-form ref="form" v-model="valid">
                        <v-row class="d-flex justify-space-between">
                          <v-col md="12" cols="12" class="py-1 px-5">
                            <div class="caption">Task Status Name</div>
                            <v-card
                              height="40px"
                              width="100%"
                              outlined
                              class="d-flex"
                              style="border: 1px solid grey"
                            >
                              <v-card-title class="py-0 pr-0">
                                <v-icon
                                  class="pr-3"
                                  size="20"
                                  style="border-right: 1px solid grey"
                                  >mdi-pencil</v-icon
                                >
                              </v-card-title>
                              <v-card-title
                                class="pa-0 px-0"
                                style="width: 100%"
                              >
                                <v-text-field
                                  type="text"
                                  class="
                                    subtitle-1
                                    align-start
                                    justify-center
                                    width-calc
                                    py-0
                                    grey--text
                                    text--darken-1
                                  "
                                  dense
                                  clearable
                                  solo
                                  :rules="rules.nameRules"
                                  required
                                  label="Task Status Name"
                                  style="outline: none; width: 100%"
                                  v-model="editedItem.name"
                                  :counter="30"
                                ></v-text-field>
                              </v-card-title>
                            </v-card>
                          </v-col>
                        </v-row>
                        <v-row class="pt-7">
                          <v-col class="pa-0 px-5">
                            <div class="d-flex align-center justify-end">
                              <div class="pr-2">
                                <v-btn
                                  width="100"
                                  depressed
                                  color="text-capitalize white--text primary"
                                  @click="save"
                                  :disabled="!valid"
                                >
                                  Save
                                </v-btn>
                              </div>
                              <div>
                                <v-btn
                                  width="100"
                                  depressed
                                  color="text-capitalize white--text red lighten-2"
                                  @click="close"
                                >
                                  Cancel
                                </v-btn>
                              </div>
                            </div>
                          </v-col>
                        </v-row>
                      </v-form>
                    </v-container>
                  </v-card-text>
                </v-card>
              </v-dialog>
              <v-dialog v-model="dialogDelete" :max-width="widthDelete">
                <v-card tile outlined class="py-3">
                  <v-card-actions>
                    <v-spacer></v-spacer>
                    <v-icon color="secondary" size="120"
                      >mdi-alert-rhombus-outline</v-icon
                    >
                    <v-spacer></v-spacer>
                  </v-card-actions>
                  <v-card-title class="px-11 pt-5 align-center justify-center">
                    <div style="font-size: 22px; color: #627080">
                      Are you sure you want to delete?
                    </div>
                    <div
                      style="font-size: 15px; color: #627080"
                      class="py-0 font-weight-regular"
                    >
                      Do you really want to delete this? This process cannot be
                      undone !!!
                    </div>
                  </v-card-title>

                  <v-card-actions class="py-4">
                    <v-spacer></v-spacer>
                    <v-btn
                      depressed
                      color="#D45252"
                      width="100"
                      class="white--text text-capitalize font-weight-regular"
                      @click="deletePointValueConfirm"
                      >Delete</v-btn
                    >
                    <v-btn
                      depressed
                      color="#578DD2"
                      width="100"
                      class="white--text text-capitalize font-weight-regular"
                      @click="closeDelete"
                      >Cancel</v-btn
                    >

                    <v-spacer></v-spacer>
                  </v-card-actions>
                </v-card>
              </v-dialog>
            </template>
            <template v-slot:[`item.actions`]="{ item }">
              <div class="d-flex">
                <v-btn
                  width="auto"
                  small
                  color="#578DD2"
                  depressed
                  class="white--text text-capitalize font-weight-regular"
                  @click="editPointValue(item)"
                >
                  Edit
                </v-btn>
                <v-spacer></v-spacer>
                <v-btn
                  @click="deletePointValue(item)"
                  small
                  depressed
                  color="#D45252"
                  width="auto"
                  class="white--text text-capitalize font-weight-regular"
                >
                  Delete
                </v-btn>
              </div>
            </template>

            <template v-slot:[`item.range`]="{ index }">
              <div>
                {{ index + 1 }}
              </div>
            </template>
          </v-data-table>
          <v-card color="grey lighten-3" outlined tile>
            <v-card-text
              class="pa-0 py-2 pa-2 d-flex align-center justify-center"
            >
              <div class="body-2 pl-2">
                Display 1 to {{ itemsPerPage }} of {{ totalItems }} Entries
              </div>
              <v-spacer></v-spacer>
              <div>
                <v-pagination
                  :total-visible="7"
                  v-model="page"
                  :length="pageCount"
                ></v-pagination>
              </div>
            </v-card-text>
          </v-card>
        </v-card>
      </v-col>
    </v-row>
    <v-row>
      <v-col>
        <ErrorPage403 v-show="errorPage" />
      </v-col>
    </v-row>
  </v-container>
</template>

<script>
import Title from "~/components/title/user_friendly.vue";
import Subtitle from "~/components/card/subtitle.vue";
import Search from "~/components/input/search.vue";
import Page from "~/components/selector/label_selector.vue";
import ErrorPage403 from "~/components/error/page_403.vue";
export default {
  data: () => ({
    errorPage: false,
    valid: true,
    rules: {
      nameRules: [
        (v) => !!v || "Task Status Name is required",
        (v) =>
          (v && v.length <= 30) ||
          "Task Status must be less than 30 characters",
      ],
    },
    search: "",
    timeout: 3000,
    snackbarAdd: false,
    snackbarUpdate: false,
    snackbarDelete: false,
    page: 1,
    pageCount: 0,
    dialog: false,
    dialogDelete: false,
    editedIndex: -1,
    editedItem: {
      value: 0,
    },
    defaultItem: {
      value: 0,
    },
    itemsPerPage: 10,
    pages: [10, 20, 50, 100],
    toggle_exclusive: undefined,
    route: [{ text: "General" }, { text: "Point Values" }],
    kit_point_value: [],
    headers: [
      { text: "", value: "range", width: "3%", align: "center" },
      { text: "Point Value", value: "value", width: "84%" },
      { text: "Actions", value: "actions", width: "3%", align: "center" },
    ],
  }),
  computed: {
    formTitle() {
      return this.editedIndex === -1 ? "Create New" : "Update";
    },
    formSubtitle() {
      return this.editedIndex === -1 ? "Register" : "Update";
    },
    totalItems() {
      return this.kit_point_value.length;
    },
    width() {
      switch (this.$vuetify.breakpoint.name) {
        case "lg":
          return "30%";
        case "xl":
          return "30%";
        case "md":
          return "50%";
        case "sm":
          return "70%";
        default:
          "50%";
      }
    },
    widthDelete() {
      switch (this.$vuetify.breakpoint.name) {
        case "lg":
          return "40%";
        case "xl":
          return "40%";
        case "md":
          return "50%";
        case "sm":
          return "70%";
        default:
          "50%";
      }
    },
  },
  mounted() {
    this.getPointValues();
  },
  methods: {
    async getPointValues() {
      try {
        let { kit_point_value } = await this.$axios.$get("/kit/point/values");
        this.kit_point_value = kit_point_value;
      } catch (e) {
        this.errorPage = true;
        this.$nuxt.setLayout("session");
      }
    },

    //Add Point Value Button
    addPointValue() {
      this.editedIndex = -1;
      this.dialog = true;
      this.editedItem = Object.assign({});
    },

    editPointValue(item) {
      this.editedIndex = this.kit_point_value.indexOf(item);
      this.editedItem = Object.assign({}, item);
      this.dialog = true;
    },
    deletePointValue(item) {
      this.editedIndex = this.kit_point_value.indexOf(item);
      this.editedItem = Object.assign({}, item);
      this.dialogDelete = true;
    },

    //save
    async save() {
      this.$refs.form.validate();
      //Update Point_Value
      if (this.editedItem > -1) {
        Object.assign(this.kit_point_value[this.editedIndex], this.editedItem);
        await this.$axios({
          method: "put",
          url: `/kit/point/value/${this.editedItem.id}`,
          data: {
            id: this.editedItem.id,
            value: this.editedItem.value,
          },
        }).then((res) => {
          this.snackbarUpdate = true;
        });
      } else {
        //Add New Point Value
        this.kit_point_value.push(this.editedItem);
        await this.$axios({
          method: "post",
          url: "/kit/point/value/create",
          data: {
            name: this.editedItem.name,
          },
        }).then((res) => {
          this.getPointValues();
          this.snackbarAdd = true;
        });
      }
      this.close();
    },

    //Delete Point Value Confirm popup
    async deletePointValueConfirm() {
      await this.$axios({
        method: "delete",
        url: `/kit/point/value/delete${this.editedItem.id}`,
      }).then((res) => {
        this.kit_point_value.splice(this.editedIndex, 1);
      });
      this.closeDelete();
      this.snackbarDelete = true;
    },

    //close popup
    close() {
      this.dialog = false;
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem);
        this.editedIndex = -1;
      });
    },

    //close delete popup
    closeDelete() {
      this.dialogDelete = false;
      this.$nextTick(() => {
        this.editedItem = Object.assign({}, this.defaultItem);
        this.editedIndex = -1;
      });
    },
  },
  components: {
    Title,
    Subtitle,
    Search,
    Page,
    ErrorPage403,
  },
};
</script>

<style>
td,
th {
  border-right: 1px solid #d1d1d1 !important;
}
.v-data-table__expanded.v-data-table__expanded__content {
  box-shadow: none !important;
}
.pagination {
  display: inline-block;
}

.v-input__slot {
  align-items: center;
  justify-content: center;
}

.pagination a {
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  transition: background-color 0.3s;
  border: 1px solid #ddd;
}

.pagination a.active {
  background-color: #4caf50;
  color: white;
  border: 1px solid #4caf50;
}

.pagination a:hover:not(.active) {
  background-color: #ddd;
}
.v-pagination__item .v-pagination__item--active .primary {
  background-color: #e3e3e3 !important;
  border-color: #b8b8b8 !important;
  color: #606060 !important;
  font-size: 18px;
}

.v-pagination__item {
  border-radius: 0;
  font-size: 1rem;
  height: 43px;
  width: 54px;
  margin: 0;
  min-width: 34px;
  border: 1px solid #b8b8b8;
  /* padding: 0 3px; */
  text-decoration: none;
  transition: 0.3s cubic-bezier(0, 0, 0.2, 1);
  box-shadow: none;
}
.v-pagination__navigation {
  box-shadow: none;
  border-radius: 0;
  display: inline-flex;
  margin: 0;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  border: 1px solid #b8b8b8;
  height: 43px;
  width: 54px;
}
</style>
